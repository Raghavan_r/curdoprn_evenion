package com.app.curdoprn

const val EDIT = "edit"
const val DELETE = "delete"

interface AdapterClick {
    fun onItemClick(position:Int,value:String)
}