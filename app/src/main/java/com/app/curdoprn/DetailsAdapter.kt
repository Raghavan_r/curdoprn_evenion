package com.app.curdoprn

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.app.curdoprn.databinding.ItemCardBinding

class DetailsAdapter(
    private val viewLifecycleOwner: LifecycleOwner,
    private val adapterClick: AdapterClick
) :
    RecyclerView.Adapter<DetailsViewHolder>() {

    private var list = emptyList<DetailsData>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemCardBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = viewLifecycleOwner
        return DetailsViewHolder(itemBinding,adapterClick)
    }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

    fun setDataList(list: List<DetailsData>) {
        this.list = list
        notifyDataSetChanged()
    }

}