package com.app.curdoprn

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.app.curdoprn.databinding.ActivityMainBinding
import com.app.curdoprn.databinding.DetailsDialogBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private val adapter by lazy {
        DetailsAdapter(this, object : AdapterClick {
            override fun onItemClick(position: Int, value: String) {
                when (value) {
                    EDIT -> {
                        val data = viewModel.getSelectedUser(position)
                        detailsDialog(data, isEdit = true, position)
                    }
                    DELETE -> {
                        viewModel.removeData(position)
                    }
                }
            }
        })
    }
    private lateinit var materialAlertDialogBuilder: MaterialAlertDialogBuilder
    private lateinit var itemBinding: DetailsDialogBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.rvDetail.adapter = adapter
        dataObserver()
    }

    private fun dataObserver() {
        viewModel.data.observe(this, {
            adapter.setDataList(it ?: emptyList())
        })

        binding.fab.setOnClickListener {
           
            detailsDialog()
        }
    }

    private fun detailsDialog(
        data: DetailsData? = null,
        isEdit: Boolean = false,
        position: Int = 0
    ) {
        materialAlertDialogBuilder = MaterialAlertDialogBuilder(this)
        val layoutInflater = LayoutInflater.from(this)
        itemBinding = DetailsDialogBinding.inflate(layoutInflater, null, false)

        if (data != null) {
            itemBinding.nameTextField.editText?.setText(data.name)
            itemBinding.tvDesignation.editText?.setText(data.detail)
        }
        materialAlertDialogBuilder.setView(itemBinding.root)
            .setTitle("Details")
            .setMessage("Enter your basic details")
            .setPositiveButton("Add") { dialog, _ ->
                val name = itemBinding.nameTextField.editText?.text.toString()
                val designation = itemBinding.tvDesignation.editText?.text.toString()
                if (isEdit) {
                    viewModel.updateData(DetailsData(designation, name), position)

                } else {
                    viewModel.addData(DetailsData(designation, name))
                }
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                displayMessage("Operation cancelled!")
                dialog.dismiss()
            }
            .show()
    }

    private fun displayMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}