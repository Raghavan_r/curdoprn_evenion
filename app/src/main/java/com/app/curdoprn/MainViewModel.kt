package com.app.curdoprn

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private val _data = MutableLiveData<List<DetailsData>>().apply {
        this.value = getSampleData()
    }
    val data : LiveData<List<DetailsData>> = _data

    private fun getSampleData(): List<DetailsData> {
        val data = emptyList<DetailsData>().toMutableList()
        data.add(DetailsData("John","Designer"))
        data.add(DetailsData("Arun","Developer"))
        data.add(DetailsData("Kishore","Tester"))
        data.add(DetailsData("Makesh","HR"))

        return data
    }

    fun updateData(updatedData: DetailsData,position: Int) {
        val data = _data.value?.toMutableList()
        data?.set(position,updatedData)
        _data.value = data
    }

    fun removeData(position: Int) {
        val data = _data.value?.toMutableList()
        data?.removeAt(position)
        _data.value = data
    }

    fun addData(newEntry: DetailsData) {
        val data = _data.value?.toMutableList()
        data?.add(newEntry)
        _data.value = data
    }

    fun getSelectedUser(position: Int) =
        _data.value?.get(position)



}