package com.app.curdoprn

import androidx.recyclerview.widget.RecyclerView
import com.app.curdoprn.databinding.ItemCardBinding


class DetailsViewHolder(private val itemBinding: ItemCardBinding,private val adapterClick: AdapterClick) : RecyclerView.ViewHolder(itemBinding.root){
    fun bind(data: DetailsData) {
        itemBinding.tvName.text = data.name
        itemBinding.tvDescription.text = data.detail

        itemBinding.ivEdit.setOnClickListener {
            adapterClick.onItemClick(adapterPosition, EDIT)
        }
        itemBinding.ivDelete.setOnClickListener {
            adapterClick.onItemClick(adapterPosition, DELETE)
        }
    }
}